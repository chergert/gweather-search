# GWeather Search

This is a simple search engine for the `Locations.bin` of GWeather. But
honestly, it doesn't matter where libgweather gets the data from.

It works by creating a trigram index on disk which can be `mmap()`d at
runtime to perform searches. The content of that index is the serialized
GWeatherLocation and a series of trigrams that appear in the
normalized/casefold'd version of some textual representation of that
location (`city_name` and `country_name`, but we could certainly add more).

That file should be saved somewhere like `Locations.search`.

Then to query the index, you load a SearchIndex from that file and search
for trigrams in your term list. At that point you only iterate the locations
which you know contain all of the trigrams.

Then you can re-inflate those `GWeatherLocation` using
`gweather_location_deserialize()` and do a final term list check to see that
they invidual items match your expected term list.

This makes search, not only very CPU efficient, but quite fast compared to
what `gnome-clocks` is doing currently.

For example, a simple query currently clocks in around `250 msec` from
`gnome-clocks --gapplication-service`. This search index query is about
`14 msec` and only gets faster the more characters you type.
