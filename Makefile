all: gweather-index gweather-search

WARNINGS := \
  -Wcast-align \
  -Wdeclaration-after-statement \
  -Werror=address \
  -Werror=array-bounds \
  -Werror=empty-body \
  -Werror=implicit \
  -Werror=implicit-function-declaration \
  -Werror=incompatible-pointer-types \
  -Werror=init-self \
  -Werror=int-conversion \
  -Werror=int-to-pointer-cast \
  -Werror=main \
  -Werror=misleading-indentation \
  -Werror=missing-braces \
  -Werror=missing-include-dirs \
  -Werror=nonnull \
  -Werror=overflow \
  -Werror=parentheses \
  -Werror=pointer-arith \
  -Werror=pointer-to-int-cast \
  -Werror=redundant-decls \
  -Werror=return-type \
  -Werror=sequence-point \
  -Werror=shadow \
  -Werror=strict-prototypes \
  -Werror=trigraphs \
  -Werror=undef \
  -Werror=write-strings \
  -Wformat=2 \
  -Wformat-nonliteral \
  -Wignored-qualifiers \
  -Wimplicit-function-declaration \
  -Wlogical-op \
  -Wmissing-declarations \
  -Wmissing-format-attribute \
  -Wmissing-include-dirs \
  -Wmissing-noreturn \
  -Wnested-externs \
  -Wno-cast-function-type \
  -Wno-dangling-pointer \
  -Wno-missing-field-initializers \
  -Wno-sign-compare \
  -Wno-unused-parameter \
  -Wold-style-definition \
  -Wpointer-arith \
  -Wredundant-decls \
  -Wstrict-prototypes \
  -Wswitch-default \
  -Wswitch-enum \
  -Wundef \
  -Wuninitialized \
  -Wunused \
  -fno-strict-aliasing

PKGS := libdex-1 gweather4

SOURCES = search-index.c
HEADERS = search-index.h

DEBUG := -ggdb -O2

gweather-index: $(SOURCES) $(HEADERS) Makefile gweather-index.c
	$(CC) $(DEBUG) -std=gnu11 -o $@.tmp $(WARNINGS) $(shell pkg-config --cflags --libs $(PKGS)) $(SOURCES) gweather-index.c
	mv $@.tmp $@

gweather-search: $(SOURCES) $(HEADERS) Makefile gweather-search.c
	$(CC) $(DEBUG) -std=gnu11 -o $@.tmp $(WARNINGS) $(shell pkg-config --cflags --libs $(PKGS)) $(SOURCES) gweather-search.c
	mv $@.tmp $@

clean:
	rm -rf *.tmp gweather-index gweather-search
