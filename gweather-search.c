/* gweather-search.c
 *
 * Copyright 2023 Christian Hergert <chergert@redhat.com>
 *
 * This library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of the
 * License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-2.1-or-later
 */

#include "config.h"

#include <libgweather/gweather.h>

#include "search-index.h"

static GBytes *
next_match (SearchIndexIter *iters,
            guint            n_iters)
{
  SearchDocument document;
  g_autoptr(GBytes) bytes = NULL;

again:
  g_clear_pointer (&bytes, g_bytes_unref);

  if (!(bytes = search_index_iter_next_bytes (&iters[0], &document)))
    return FALSE;

  for (guint i = 1; i < n_iters; i++)
    {
      if (!search_index_iter_seek_to (&iters[i], document.id))
        goto again;
    }

  return g_steal_pointer (&bytes);
}

static GVariant *
deserialize_bytes (GBytes *bytes)
{
  GVariant *variant = g_variant_new_from_bytes (G_VARIANT_TYPE ("(uv)"), bytes, FALSE);
  g_bytes_unref (bytes);
  return variant;
}

static gboolean
gweather_search_matches (GWeatherLocation   *location,
                         const char * const *terms,
                         guint               n_terms)
{
  const char *city_name = gweather_location_get_city_name (location);
  const char *country_name = gweather_location_get_country_name (location);

  g_autofree char *city_name_normal = city_name ? g_utf8_normalize (city_name, -1, G_NORMALIZE_DEFAULT) : NULL;
  g_autofree char *city_name_casefold = city_name_normal ? g_utf8_casefold (city_name_normal, -1) : NULL;

  g_autofree char *country_name_normal = country_name ? g_utf8_normalize (country_name, -1, G_NORMALIZE_DEFAULT) : NULL;
  g_autofree char *country_name_casefold = country_name_normal ? g_utf8_casefold (country_name_normal, -1) : NULL;

  for (guint i = 0; i < n_terms; i++)
    {
      if (city_name_casefold && strstr (city_name_casefold, terms[i]))
        continue;

      if (country_name_casefold && strstr (country_name_casefold, terms[i]))
        continue;

      return FALSE;
    }

  return TRUE;
}

static int
sort_by_size_asc (gconstpointer a,
                  gconstpointer b)
{
  const SearchIndexIter *iter_a = a;
  const SearchIndexIter *iter_b = b;
  gsize size_a = iter_a->end - iter_a->pos;
  gsize size_b = iter_b->end - iter_b->pos;

  if (size_a < size_b)
    return -1;
  else if (size_a > size_b)
    return 1;
  else
    return 0;
}

static void
gweather_search (GWeatherLocation   *world,
                 SearchIndex        *index,
                 const char * const *terms,
                 guint               n_terms)
{
  g_autoptr(GArray) iters = g_array_new (FALSE, FALSE, sizeof (SearchIndexIter));
  g_autoptr(GHashTable) seen = g_hash_table_new (NULL, NULL);
  g_autoptr(GPtrArray) casefold_terms = g_ptr_array_new_with_free_func (g_free);
  GBytes *bytes;

  for (guint i = 0; i < n_terms; i++)
    {
      g_autofree char *query_normal = NULL;
      g_autofree char *query_casefold = NULL;

      query_normal = g_utf8_normalize (terms[i], -1, G_NORMALIZE_DEFAULT);
      query_casefold = g_utf8_casefold (query_normal, -1);

      g_ptr_array_add (casefold_terms, g_steal_pointer (&query_casefold));
    }

  for (guint i = 0; i < casefold_terms->len; i++)
    {
      const char *query = g_ptr_array_index (casefold_terms, i);
      SearchTrigramIter iter;
      SearchTrigram trigram;

      search_trigram_iter_init (&iter, query, -1);
      while (search_trigram_iter_next (&iter, &trigram))
        {
          SearchIndexIter index_iter;
          guint encoded = search_trigram_encode (&trigram);

          if (g_hash_table_contains (seen, GUINT_TO_POINTER (encoded)))
            continue;

          search_index_iter_init (&index_iter, index, &trigram);
          g_array_append_val (iters, index_iter);
          g_hash_table_add (seen, GUINT_TO_POINTER (encoded));
        }
    }

  if (iters->len == 0)
    return;

  /* Sort the iters by the shortest one first so we avoid
   * looking at documents we are likely to skip.
   */
  g_array_sort (iters, sort_by_size_asc);

  while ((bytes = next_match (&g_array_index (iters, SearchIndexIter, 0), iters->len)))
    {
      g_autoptr(GVariant) variant = deserialize_bytes (g_steal_pointer (&bytes));

      if (variant != NULL)
        {
          g_autoptr(GWeatherLocation) location = gweather_location_deserialize (world, variant);

          /* At this point, it's pretty likely we're going to have a match because
           * we at least know that the location contains all of the characters that
           * we saw in our term list. They may not be in sequential order though, and
           * so that would be a negative match.
           */

          if (gweather_search_matches (location, (const char * const *)casefold_terms->pdata, casefold_terms->len))
            g_print ("%s : %s\n",
                     gweather_location_get_country_name (location),
                     gweather_location_get_city_name (location));
        }
    }
}

int
main (int   argc,
      char *argv[])
{
  g_autoptr(GWeatherLocation) world = NULL;
  g_autoptr(SearchIndex) index = NULL;
  g_autoptr(GError) error = NULL;
  g_autoptr(GTimer) timer = NULL;
  const char *filename;

  if (argc < 3)
    {
      g_printerr ("usage: %s SEARCH_INDEX QUERY\n", argv[0]);
      return 1;
    }

  filename = argv[1];
  if (!(index = search_index_new (filename, &error)))
    g_error ("Failed to load index: %s", error->message);

  if (!(world = gweather_location_get_world ()))
    g_error ("Failed to load GWeather world");

  timer = g_timer_new ();
  gweather_search (world, index, (const char * const *)&argv[2], argc-2);
  g_printerr ("Search completed in %u milliseconds\n", (guint)(g_timer_elapsed (timer, NULL) * 1000));

  return 0;
}
